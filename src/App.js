import React from 'react';
import Product from './Components/Product/Product';

import './Styles/App.scss';
class App extends React.Component {
  
  state = {
    products: [
      {id: 1, title: 'Book 1', price: 99},
      {id: 2, title: 'Book 2', price: 89},
      {id: 3, title: 'Book 3', price: 79},
    ],
    productsState: false,
  }

  toggleProductsHandler = () => {
    this.setState({ 
      productsState: !this.state.productsState
    })
  }

  deleteProductHandler = (productIndex) => {
    const products = [...this.state.products]
    products.splice(productIndex, 1)
    this.setState({
      products: products,
    })
  }

  productTitleHandler = (event, id) => {
    const productIndex = this.state.products.findIndex((item) => {
      return item.id === id
    })
    
    const product = {
      ...this.state.products[productIndex]
    }

    product.title = event.target.value
    const products = [...this.state.products]
    products[productIndex] = product

    this.setState({
      products: products,
    })
  }

  render() {
    const btn = {
      backgroundColor: 'rgb(0, 153, 255)',
      color: 'white',
      borderRadius: '5px',
      border: 'none',
      margin: '3px auto',
      padding: '5px'
    }

    let productsContent = null;

    if(this.state.productsState) {
      productsContent = (
        <div className='products'>
          {this.state.products.map( (item, index) => {
            return (
              <Product 
              title={item.title}
              price={item.price}
              click={() => {this.deleteProductHandler(index)}}
              key={item.id}
              change={(event) => this.productTitleHandler(event, item.id)}
              />
            )
          })}
        </div>
      )
    }

    let buttonState = "Show";
    if(this.state.productsState) buttonState = "Hide";
    if (this.state.products.length === 0) buttonState = "No";
    return(
      <div className='center'>
        <h1> Book Store </h1>
        <button style={btn} onClick={() => this.toggleProductsHandler()}>
          {buttonState} Products
        </button>
        {productsContent}
      </div>
    )
  };
};

export default App;